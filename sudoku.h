#ifndef SUDOKU_H
#define SUDOKU_H
#include <iostream>
#include <vector>
#include "generointi.h"

class sudoku
{
public:
    sudoku(std::vector<std::vector<int>> ratkaistavaSudoku);
    std::vector<int> haePystyrivinKaytetytLuvut(int haluttuPystyrivi,
                                   std::vector<std::vector<int>> haluttuSudoku);
    std::vector<int> haeNelionKaytetytLuvut(int haluttuNelio,
                                   std::vector<std::vector<int>> haluttuSudoku);
    std::vector<int> haeVaakarivinKaytetytLuvut(int haluttuRivi,
                                   std::vector<std::vector<int>> haluttuSudoku);
    bool onkoNeliossa(int luku, int haluttuNelio,
                      std::vector<std::vector<int>> haluttuSudoku);
    bool onkoRivilla(int luku, int haluttuRivi,
                     std::vector<std::vector<int>> haluttuSudoku);
    bool onkoSarakkeessa(int luku, int haluttuSarake,
                         std::vector<std::vector<int>> haluttuSudoku);
    double haeSarakeHyvyysarvo(std::vector<std::vector<int>> tulostettavaSudoku);
    double haeNelioHyvyysarvo(std::vector<std::vector<int>> tulostettavaSudoku);
    double haeHyvyysarvo(std::vector<std::vector<int>> tulostettavaSudoku);
    void tulosta(std::vector<std::vector<int>> tulostettavaSudoku);
    std::vector<std::vector<int>> haeRatkaistavaSudoku();
    void ratkaiseSudoku(int kierrokset, int eliittienMaara,
                        int populaationSuuruus, double mutaatioSuodatin,
                        double valintaArvo, int maksimiSamaHyvyysarvo);
    std::vector<std::vector<int>> valinta();
    void laskeEliittisuodatin(
            std::vector<std::vector<std::vector<int> > > populaatio);
    std::vector<std::vector<int>> mutaatio(
            std::vector<std::vector<int>> mutatoitavaSudoku);

private:
    std::vector<std::vector<int>> ratkaistavaSudoku_;
    std::vector<std::vector<std::vector<int>>> populaatio_;
    std::vector<std::vector<std::vector<int>>> eliitit_;
    std::vector<std::vector<std::vector<int>>> uusiSukupolvi_;
    generointi generointiLuokka_;
    double suurinHyvyysarvo_=0.0;
    double edellinenSuurinHyvyysarvo_=0.0;
    std::vector<std::vector<int>> parasRatkaisu_;
    //Määritellään vakiot.
    double mutaatioSuodatin_ = 0.85;
    int populaationSuuruus_ = 2000;
    int kandidaattienMaara_= 2000;
    double valintaArvo_ = 0.85;
    double eliittiSuodatin_ = 0.30;
    double eliittimaara_ = 200;
    int kierrokset_ = 100;
    int kierroksetSamaHyvyysarvoMaks_ = 100;
};

#endif // SUDOKU_H
