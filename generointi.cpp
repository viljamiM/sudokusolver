#include "generointi.h"
#include "sudoku.h"
#include <vector>


generointi::generointi()
{

}

std::vector<std::vector<int> > generointi::generoiUusiSudoku()
{
    int k = 0;
    std::vector<int> kaytetytAlkiot;
    std::vector<std::vector<int>> uusiSudoku = ratkaistavaSudoku_;
    while(k<ratkaistavaSudoku_.size()){
        int j = 0;
        kaytetytAlkiot.clear();
        while(j<ratkaistavaSudoku_.at(k).size()){
            int alkio;
            while(true){
                alkio = rand() % 9 + 1;
                int h = 0;
                bool kaytetty = false;
                while(h<kaytetytAlkiot.size()){
                    if(kaytetytAlkiot.at(h)==alkio){
                        kaytetty = true;
                    }
                    h=h+1;
                }
                if(kaytetty == false){
                    kaytetytAlkiot.push_back(alkio);
                    break;
                }
            }
            uusiSudoku.at(k).at(j)=alkio;
            j=j+1;
        }
        k=k+1;
    }
    return uusiSudoku;
}

void generointi::tallennaRatkaistavaSudoku(
        std::vector<std::vector<int> >ratkaistavaSudoku)
{
    ratkaistavaSudoku_ = ratkaistavaSudoku;
}

std::pair<std::vector<std::vector<int> >,std::vector<std::vector<int>>>
generointi::risteytys(std::vector<std::vector<int> > vanhempi1,
                      std::vector<std::vector<int> > vanhempi2)
{
    std::vector<std::vector<int> > lapsi1 = vanhempi1;
    std::vector<std::vector<int> > lapsi2 = vanhempi2;
    int aloitusPiste;
    int lopetusPiste;
    while(true){
        aloitusPiste = rand() % 8;
        lopetusPiste = rand() % 8 + 1;
        if(aloitusPiste!=lopetusPiste){
            if(lopetusPiste<aloitusPiste){
                int apu = aloitusPiste;
                aloitusPiste = lopetusPiste;
                lopetusPiste = apu;
            }
            break;
        }
    }
    int rivi = rand() % 8;
    std::vector<int> risteytettyRivi1=lapsi1.at(rivi);
    std::vector<int> risteytettyRivi2=lapsi2.at(rivi);
    lapsi1.at(rivi).clear();
    lapsi2.at(rivi).clear();
    int j = 0;
    while(j<risteytettyRivi1.size()){
       risteytettyRivi1.at(j)=0;
       j=j+1;
    }
    j=0;
    while(j<risteytettyRivi2.size()){
       risteytettyRivi2.at(j)=0;
       j=j+1;
    }
    int h = aloitusPiste;
    while(h<=lopetusPiste){
       risteytettyRivi1.at(h)=vanhempi1.at(rivi).at(h);
       h=h+1;
    }
    while(h<=lopetusPiste){
       risteytettyRivi2.at(h)=vanhempi1.at(rivi).at(h);
       h=h+1;
    }
    h=lopetusPiste+1;
    if(h>=vanhempi2.at(rivi).size()){
       h=0;
    }
    int tayttokohata = lopetusPiste+1;
    if(tayttokohata>=vanhempi2.at(rivi).size()){
        tayttokohata=0;
    }
    while(true){
            int k = 0;
            bool onkoRivilla = false;
            while (k<risteytettyRivi1.size()) {
                if(risteytettyRivi1.at(k)==vanhempi2.at(rivi).at(h)){
                    onkoRivilla = true;
                    break;
                }
                k=k+1;
            }
            if(onkoRivilla==false){
                risteytettyRivi1.at(tayttokohata)=vanhempi2.at(rivi).at(h);
                tayttokohata = tayttokohata+1;
                if(tayttokohata>=vanhempi2.at(rivi).size()){
                    tayttokohata=0;
                }
            }
            if(h>=vanhempi2.at(rivi).size()-1){
                h=-1;
            }
            int nollienMaara=0;
            int u = 0;
            while(u<risteytettyRivi1.size()){
                if(risteytettyRivi1.at(u)==0){
                    nollienMaara = nollienMaara+1;
                }
                u=u+1;
            }
            if(nollienMaara==0){
                break;
            }
            h=h+1;
    }
    h=lopetusPiste+1;
    if(h>=vanhempi1.at(rivi).size()){
       h=0;
    }
    tayttokohata = lopetusPiste+1;
    if(tayttokohata>=vanhempi1.at(rivi).size()){
        tayttokohata=0;
    }
    while(true){
            int k = 0;
            bool onkoRivilla = false;
            while (k<risteytettyRivi2.size()) {
                if(risteytettyRivi2.at(k)==vanhempi1.at(rivi).at(h)){
                    onkoRivilla = true;
                    break;
                }
                k=k+1;
            }
            if(onkoRivilla==false){
                risteytettyRivi2.at(tayttokohata)=vanhempi1.at(rivi).at(h);
                tayttokohata = tayttokohata+1;
                if(tayttokohata>=vanhempi1.at(rivi).size()){
                    tayttokohata=0;
                }
            }
            if(h>=vanhempi1.at(rivi).size()-1){
                h=-1;
            }
            int nollienMaara=0;
            int u = 0;
            while(u<risteytettyRivi2.size()){
                if(risteytettyRivi2.at(u)==0){
                    nollienMaara = nollienMaara+1;
                }
                u=u+1;
            }
            if(nollienMaara==0){
                break;
            }
            h=h+1;
    }
    lapsi1.at(rivi) = risteytettyRivi1;
    lapsi2.at(rivi) = risteytettyRivi2;
    std::vector<std::vector<int> > korjattuLapsi1 = sudokunKorjaus(lapsi1);
    std::vector<std::vector<int> > korjattuLapsi2 = sudokunKorjaus(lapsi2);
    std::pair<std::vector<std::vector<int> >,std::vector<std::vector<int> >> Pair1;
    Pair1.first = korjattuLapsi1;
    Pair1.second = korjattuLapsi2;
    return Pair1;
}

std::vector<std::vector<int> > generointi::sudokunKorjaus(
        std::vector<std::vector<int> > korjattavaSudoku)
{
    std::vector<std::vector<int>> alkuperainenSudoku
            = ratkaistavaSudoku_;
    int k = 0;
    while(k<ratkaistavaSudoku_.size()){
        int j = 0;
        while(j<ratkaistavaSudoku_.at(k).size()){
            if(alkuperainenSudoku.at(j).at(k)!=0
                    and alkuperainenSudoku.at(j).at(k)!=korjattavaSudoku.at(j).at(k)){
                int h = 0;
                while(h<alkuperainenSudoku.at(j).size()){
                    if(alkuperainenSudoku.at(j).at(k)==korjattavaSudoku.at(j).at(h)){
                        int vaihdettavaAlkio = korjattavaSudoku.at(j).at(k);
                        korjattavaSudoku.at(j).at(k) = korjattavaSudoku.at(j).at(h);
                        korjattavaSudoku.at(j).at(h) = vaihdettavaAlkio;
                        break;
                    }
                    h=h+1;
                }
            }
            j=j+1;
        }
        k=k+1;
    }
    return korjattavaSudoku;
}
