#ifndef GENEROINTI_H
#define GENEROINTI_H
#include <vector>

class generointi
{
public:
    generointi();
    std::vector<std::vector<int>>generoiUusiSudoku();
    std::vector<std::vector<int>>sudokunKorjaus(
            std::vector<std::vector<int> > korjattavaSudoku);
    void tallennaRatkaistavaSudoku(
            std::vector<std::vector<int>> ratkaistavaSudoku);

    std::pair<std::vector<std::vector<int>>,std::vector<std::vector<int> >>
    risteytys(
            std::vector<std::vector<int>> vanhempi1,
            std::vector<std::vector<int>> vanhempi2);
private:
    std::vector<std::vector<int>> ratkaistavaSudoku_;

};

#endif // GENEROINTI_H
