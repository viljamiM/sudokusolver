#include "sudoku.h"
#include "generointi.h"
#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <iterator>

sudoku::sudoku(std::vector<std::vector<int>> ratkaistavaSudoku):
    ratkaistavaSudoku_(ratkaistavaSudoku)
{
    generointiLuokka_ = generointi();
    generointiLuokka_.tallennaRatkaistavaSudoku(ratkaistavaSudoku_);
}
std::vector<int> sudoku::haePystyrivinKaytetytLuvut(
        int haluttuPystyrivi,std::vector<std::vector<int>> haluttuSudoku){
    std::vector<int> kaytetytLuvut;
    int k = 0;
    while(k<haluttuSudoku.at(1).size()){
        if(haluttuSudoku.at(k).at(haluttuPystyrivi)!=0){
            kaytetytLuvut.push_back(haluttuSudoku.at(k).at(haluttuPystyrivi));
        }
        k=k+1;
    }
    return kaytetytLuvut;
}
std::vector<int> sudoku::haeNelionKaytetytLuvut(
        int haluttuNelio,std::vector<std::vector<int>> haluttuSudoku){
    std::vector<int> kaytetytLuvut;
    int k;
    if(haluttuNelio==1){
        k=0;
        while (k<3) {
            int i = 0;
            while(i<3){
                if(haluttuSudoku.at(k).at(i)){
                    kaytetytLuvut.push_back(haluttuSudoku.at(k).at(i));
                }
                i=i+1;
            }
            k=k+1;
        }
    }
    if(haluttuNelio==2){
        k=0;
        while (k<3) {
            int i = 3;
            while(i<6){
                if(haluttuSudoku.at(k).at(i)){
                    kaytetytLuvut.push_back(haluttuSudoku.at(k).at(i));
                }
                i=i+1;
            }
            k=k+1;
        }

    }
    if(haluttuNelio==3){
        k=0;
        while (k<3) {
            int i = 6;
            while(i<9){
                if(haluttuSudoku.at(k).at(i)){
                    kaytetytLuvut.push_back(haluttuSudoku.at(k).at(i));
                }
                i=i+1;
            }
            k=k+1;
        }
    }
    if(haluttuNelio==4){
        k=3;
        while (k<6) {
            int i = 0;
            while(i<3){
                if(haluttuSudoku.at(k).at(i)){
                    kaytetytLuvut.push_back(haluttuSudoku.at(k).at(i));
                }
                i=i+1;
            }
            k=k+1;
        }
    }
    if(haluttuNelio==5){
        k=3;
        while (k<6) {
            int i = 3;
            while(i<6){
                if(haluttuSudoku.at(k).at(i)){
                    kaytetytLuvut.push_back(haluttuSudoku.at(k).at(i));
                }
                i=i+1;
            }
            k=k+1;
        }
    }
    if(haluttuNelio==6){
        k=3;
        while (k<6) {
            int i = 6;
            while(i<9){
                if(haluttuSudoku.at(k).at(i)){
                    kaytetytLuvut.push_back(haluttuSudoku.at(k).at(i));
                }
                i=i+1;
            }
            k=k+1;
        }
    }
    if(haluttuNelio==7){
        k=6;
        while (k<9) {
            int i = 0;
            while(i<3){
                if(haluttuSudoku.at(k).at(i)){
                    kaytetytLuvut.push_back(haluttuSudoku.at(k).at(i));
                }
                i=i+1;
            }
            k=k+1;
        }
    }
    if(haluttuNelio==8){
        k=6;
        while (k<9) {
            int i = 3;
            while(i<6){
                if(haluttuSudoku.at(k).at(i)){
                    kaytetytLuvut.push_back(haluttuSudoku.at(k).at(i));
                }
                i=i+1;
            }
            k=k+1;
        }
    }
    if(haluttuNelio==9){
        k=6;
        while (k<9) {
            int i = 6;
            while(i<9){
                if(haluttuSudoku.at(k).at(i)){
                    kaytetytLuvut.push_back(haluttuSudoku.at(k).at(i));
                }
                i=i+1;
            }
            k=k+1;
        }
    }
    return kaytetytLuvut;
}
std::vector<int> sudoku::haeVaakarivinKaytetytLuvut(
        int haluttuRivi,std::vector<std::vector<int>> haluttuSudoku){
    std::vector<int> kaytetytLuvut;
    int k = 0;
    while(k<haluttuSudoku.at(haluttuRivi).size()){
        if(haluttuSudoku.at(haluttuRivi).at(k)!=0){
            kaytetytLuvut.push_back(haluttuSudoku.at(haluttuRivi).at(k));
        }
        k=k+1;
    }
    return kaytetytLuvut;
}
bool sudoku::onkoNeliossa(int luku, int haluttuNelio,
                          std::vector<std::vector<int>> haluttuSudoku){
    std::vector<int> sisaltavatNumerot = haeNelionKaytetytLuvut(haluttuNelio,haluttuSudoku);
    int j = 0;
    bool sisaltyyko = false;
    while(j<sisaltavatNumerot.size()){
        if(sisaltavatNumerot.at(j)==luku){
            sisaltyyko = true;
        }
        j=j+1;
    }
    return sisaltyyko;
}
bool sudoku::onkoRivilla(int luku, int haluttuRivi,
                         std::vector<std::vector<int>> haluttuSudoku){
     std::vector<int> sisaltavatNumerot = haeVaakarivinKaytetytLuvut(haluttuRivi,haluttuSudoku);
     int j = 0;
     bool sisaltyyko = false;
     while(j<sisaltavatNumerot.size()){
         if(sisaltavatNumerot.at(j)==luku){
             sisaltyyko = true;
         }
         j=j+1;
     }
     return sisaltyyko;
}
bool sudoku::onkoSarakkeessa(int luku, int haluttuSarake,
                             std::vector<std::vector<int>> haluttuSudoku){
     std::vector<int> sisaltavatNumerot =
             haePystyrivinKaytetytLuvut(haluttuSarake,haluttuSudoku);
     int j = 0;
     bool sisaltyyko = false;
     while(j<sisaltavatNumerot.size()){
         if(sisaltavatNumerot.at(j)==luku){
             sisaltyyko = true;
         }
         j=j+1;
     }
     return sisaltyyko;
}
void sudoku::tulosta(std::vector<std::vector<int>> tulostettavaSudoku){
    //std::cout<<"Tulostetaan ratkaisuehdotus: "<<std::endl;
    unsigned int i=0;
    while(i<tulostettavaSudoku.size()){
        unsigned int k=0;
        while(k<tulostettavaSudoku.at(i).size()){
            std::cout<<tulostettavaSudoku.at(i).at(k)<<" ";
            ++k;
        }
        std::cout<<""<<std::endl;
        ++i;
    }
}

std::vector<std::vector<int>> sudoku::haeRatkaistavaSudoku()
{
    return ratkaistavaSudoku_;
}
void sudoku::ratkaiseSudoku(int kierrokset, int eliittienMaara,
                            int populaationSuuruus, double mutaatioSuodatin,
                            double valintaArvo, int maksimiSamaHyvyysarvo)
{
    //Alustetaan vakiot.
    kierrokset_ = kierrokset;
    eliittimaara_ = eliittienMaara;
    populaationSuuruus_ = populaationSuuruus;
    kandidaattienMaara_ = populaationSuuruus;
    mutaatioSuodatin_ = mutaatioSuodatin;
    valintaArvo_ = valintaArvo;
    kierroksetSamaHyvyysarvoMaks_ = maksimiSamaHyvyysarvo;

    //tulostataan ratkaistava sudoku
    tulosta(ratkaistavaSudoku_);

    //Luodaan populaatio ja tallennetaan se vectoriin.
    std::vector<std::vector<std::vector<int>>> populaatio;
    int h = 0;
    std::vector<std::vector<std::vector<int>>> eliitit;
    int laskuri=0;
    while(h<populaationSuuruus_){
        std::vector<std::vector<int>> generoituSudoku
                = generointiLuokka_.sudokunKorjaus(
                    generointiLuokka_.generoiUusiSudoku());
        populaatio.push_back(generoituSudoku);
        h=h+1;
    }
    populaatio_ = populaatio;
    int t = 0;

    //Aloitetaan ratkaisun etsiminen
    while(t<kierrokset_){

        //Lasketaan eliittisuodattimen arvo populaation hyvyysarvojen perusteella
        laskeEliittisuodatin(populaatio_);
        h=0;
        bool muuttuikoHyvyysarvo = false;

        //Karsitaan populaatiosta eliitit ja tallennetaan suurin hyvyysarvo muistiin
        while(h<populaatio_.size()){
            double hyvyysarvo = haeHyvyysarvo(populaatio_.at(h));
            if(hyvyysarvo>=eliittiSuodatin_){
                eliitit.push_back(populaatio_.at(h));
            }
            if(hyvyysarvo>suurinHyvyysarvo_){
                muuttuikoHyvyysarvo = true;
                laskuri = 0;
                edellinenSuurinHyvyysarvo_ = suurinHyvyysarvo_;
                suurinHyvyysarvo_=hyvyysarvo;
                parasRatkaisu_ = populaatio_.at(h);
            }
            ++h;
        }

        //Pidetään kirjaa kuinka monta sukupolvea on suurin hyvyysarvo pysynyt samana
        if(muuttuikoHyvyysarvo == false){
            laskuri = laskuri + 1;
        }

        eliitit_ = eliitit;
        populaatio_.clear();
        populaatio_=eliitit_;
        uusiSukupolvi_.clear();
        uusiSukupolvi_ = eliitit;
        eliitit.clear();

        //Luodaan uusi sukupolvi lisäämällä eliitit uuteen sukupolveen ja
        //risteytetään loput sukupolvesta eliiteistä.

        while(uusiSukupolvi_.size()<kandidaattienMaara_){

                //valitaan lasten vanhemmat satunnaisesti eliittien joukosta
                std::vector<std::vector<int>> vanhempi1;
                std::vector<std::vector<int>> vanhempi2;
                if(populaatio_.size()>1){
                    vanhempi1 = valinta();
                    vanhempi2 = valinta();
                }
                else{
                    vanhempi1 = populaatio_.at(0);
                    vanhempi2 = populaatio_.at(0);
                }

                //risteytetään lapset vanhemmista
                std::pair<std::vector<std::vector<int>>,
                        std::vector<std::vector<int>>> lapset
                        = generointiLuokka_.risteytys(vanhempi1,vanhempi2);

                //Mutatoidaan lapset
                std::vector<std::vector<int>> mutatoituLapsi1 =
                        mutaatio(lapset.first);
                std::vector<std::vector<int>> mutatoituLapsi2 =
                        mutaatio(lapset.second);

                //Lisätään lapset uuteen sukupolveen
                uusiSukupolvi_.push_back(mutatoituLapsi1);
                uusiSukupolvi_.push_back(mutatoituLapsi2);
         }
         populaatio_=uusiSukupolvi_;

         //Mikäli suurin hyvyysarvo on pysynyt samana liian monta kierrosta luodaan uusi populaatio
         if(laskuri>=kierroksetSamaHyvyysarvoMaks_){
                std::cout<<"Suurin hyvyysarvo sukupolvella "+std::to_string(t)
                           +". on: "+std::to_string(suurinHyvyysarvo_)<<std::endl;
                std::cout<<"Luodaan uusi populaatio, koska suurin hyvyysarvo "
                           "ollut liian monta kierrosta sama"<<std::endl;
                populaatio.clear();
                laskuri = 0;
                suurinHyvyysarvo_ = 0.0;
                h=0;
                while(h<populaationSuuruus_){
                    std::vector<std::vector<int>> generoituSudoku
                            = generointiLuokka_.sudokunKorjaus(
                                generointiLuokka_.generoiUusiSudoku());
                    populaatio.push_back(generoituSudoku);
                    h=h+1;
                }
                populaatio_ = populaatio;
         }
         t=t+1;

         //Etsiminen lopetetaan, kun suurin hyvyysarvo on 1.0 eli ratkaisu on löytynyt
         if(suurinHyvyysarvo_==1.0){
             break;
         }
    }
    if(suurinHyvyysarvo_==1.0){
        std::cout<<"Ratkaisu loydettiin ja se on:"<<std::endl;
    }
    else{
       std::cout<<"Ratkaisua ei loydetty 100. sukupolven aikana, "
                  "muuta parametreja."
                  " \n"
                  "parhaimmaksi ratkaisuksi saatiin:"<<std::endl;
    }
    tulosta(parasRatkaisu_);
}

std::vector<std::vector<int> > sudoku::valinta()
{
    int ehdokaan1numero;
    int ehdokaan2numero;
    int kandidaattienMaaranMuunnos = populaatio_.size()-1;

    //Arvotaan molempien ehdokkaiden numerot
    while(true){
        ehdokaan1numero = rand() & kandidaattienMaaranMuunnos;
        ehdokaan2numero = rand() & kandidaattienMaaranMuunnos;
        if(ehdokaan1numero!=ehdokaan2numero){
            break;
        }
    }

    //Haetaan numeroa vastaavat ehdokkaat populaatiosta.
    std::vector<std::vector<int>> ehdokas1 = populaatio_.at(ehdokaan1numero);
    std::vector<std::vector<int>> ehdokas2 = populaatio_.at(ehdokaan2numero);

    //Haetaan ehdokkaiden hyvyysarvot
    double ehdokkaa1hyvyys = haeHyvyysarvo(ehdokas1);
    double ehdokkaan2hyvyys = haeHyvyysarvo(ehdokas2);
    std::vector<std::vector<int>> parempi;
    std::vector<std::vector<int>> heikompi;

    //Tutkitaan kumpi ehdokkaista on parempi ja kumpi heikompi
    if(ehdokkaa1hyvyys>=ehdokkaan2hyvyys){
        parempi = ehdokas1;
        heikompi = ehdokas2;
    }
    else{
        parempi = ehdokas2;
        heikompi = ehdokas1;
    }

    //Arvotaan sattumanvarainen desimaaliluku väliltä 0-1
    std::uniform_real_distribution<double> unif(0,1);
    std::default_random_engine re;
    double valintaNumero = unif(re);

    //Mikäli arvottu luku on pienempää kuin ns. valinta arvo, niin valitaan parempi,
    //muussa tapauksessa valitaan heikompi ehdokas.
    if(valintaNumero<valintaArvo_){
        return parempi;
    }
    else{
        return heikompi;
    }
}

void sudoku::laskeEliittisuodatin(
        std::vector<std::vector<std::vector<int> >> populaatio2)
{
    /*
      Lasketaan populaation hyvyysarvojen pohjalta sellainen eliittisuodatin,
      että populaatiosta suoraan jatkoon pääsee vain haluttu määrä eliittejä.

    */
    std::vector<double> hyvyysarvotvektori;
    unsigned int h=0;
    while(h<populaatio2.size()){
        double hyvyysarvo=haeHyvyysarvo(populaatio2.at(h));
        hyvyysarvotvektori.push_back(hyvyysarvo);
        h=h+1;
    }

    double hyvyysarvosumma=0;
    unsigned int j=0;
    while(j<hyvyysarvotvektori.size()){
        hyvyysarvosumma=hyvyysarvosumma+hyvyysarvotvektori.at(j);
        ++j;
    }
    int montako = hyvyysarvotvektori.size();
    //Järjestetään vektori pienimmästä suurimpaan.
    std::sort(hyvyysarvotvektori.begin(),hyvyysarvotvektori.end());

    double tulos= hyvyysarvotvektori.at(montako-eliittimaara_);

    eliittiSuodatin_=tulos;

}

std::vector<std::vector<int> > sudoku::mutaatio(std::vector<std::vector<int> >
                                                mutatoitavaSudoku)
{
    //Arvotaan satunnainen desimaaliluku väliltä 0-1
    std::uniform_real_distribution<double> unif(0,1);
    std::default_random_engine re;
    double valintaNumero = unif(re);

    //Mikäli arvottu luku on pienempää kuin ns. mutaatio suodattimen arvo, niin
    //lapselle tapahtuu mutaatio.
    if(valintaNumero<mutaatioSuodatin_){

        //arvotaan mutatoitava rivi.
        int rivi = rand() % 9;
        int ensimmainenAlkio;
        int toinenAlkio;
        while(true){

            //arvotaan vaihdettavat alkiot.
            ensimmainenAlkio = rand() % 9;
            toinenAlkio = rand() % 9;
            if(ensimmainenAlkio!=toinenAlkio){

                //Tarkistetaan, ettei luvut ole ennakkoon asetetut vakiot
                //alkuperäisessä sudokussa
                if(ratkaistavaSudoku_.at(rivi).at(ensimmainenAlkio)==0){
                    if(ratkaistavaSudoku_.at(rivi).at(toinenAlkio)==0){
                        break;
                    }
                }
            }
        }

        //Vaihdetaan alkioiden paikat rivillä
        int apuMuisti = mutatoitavaSudoku.at(rivi).at(ensimmainenAlkio);
        mutatoitavaSudoku.at(rivi).at(ensimmainenAlkio) =
                mutatoitavaSudoku.at(rivi).at(toinenAlkio);
        mutatoitavaSudoku.at(rivi).at(toinenAlkio) = apuMuisti;
        return mutatoitavaSudoku;
    }
    else{
        return mutatoitavaSudoku;
    }
}

double sudoku::haeSarakeHyvyysarvo(std::vector<std::vector<int>> tulostettavaSudoku)

{
    /* Sudoku on ratkaistu, jos:
     * 1. Jokaisella rivillä esiintyy numerot 1-9 kerran ja vain kerran. Eli rivisumma 45.
     * 2. Jokaisessa sarakkeessa esiintyy numerot 1-9 kerran ja vain kerran. Eli sarakesumma 45.
     * 3. Jokaisessa 3x3 osamatriisissa sisältyy numerot 1-9 kerran ja vain kerran. Eli osasumma 45.

    Muodostetaan näiden ehtojen pohjalta ratkaisuehdotukselle hyvyysarvo.
    Ratkaisuehdotuksen generoinnissa on jo huomioitu, ettei yhdellä rivillä ole tietty numero kuin kerran,
    näin ollen ehtoa 1. ei tarvitse huomioida.

    1. Tutkitaan ensin, onko jokaisessa sarakkeessa kaikki luvut 1-9 kerran ja vain kerran.
    Jos luku löytyy kerran, lisätään sarakeOKsummaan +1. Tämän jälkeen lasketaan kaikkien ratkaisuehdokas sudokun sarakkeiden
    summat yhteen ja lasketaan tämän perusteella sarakehyvyys=saatu summa/81.
    */
    std::vector<int> sudokuOK;
    unsigned int n=0;

    while(n<tulostettavaSudoku.size()){
        std::vector<int> sarakeOK;
        std::vector<int> sarakeluvut=haePystyrivinKaytetytLuvut(
                    n,tulostettavaSudoku); //Hakee sudokusta tietyn sarakkeen.

        //Tutkii löytyykö numerot 1-9 tasan kerran.
        unsigned int numero=1;
        while(numero<=9){
            unsigned int j=0;
            int montako=0;
            while(j<sarakeluvut.size()){
                if(sarakeluvut.at(j)==numero){
                    //Jos esim. numero 1 löytyy sarakkeelta, lisätään ++montako.
                    ++montako;
                }
                ++j;
            }
            if(montako==1){
                //Jos tietty numero löytyy sarakkeesta kerran, tallennetaan 1.
                sarakeOK.push_back(1);
            }
            else{
                //Jos tietty numero löytyy sarakkeesta jotain muuta kuin tasan kerran, tallennetaan 0.
                sarakeOK.push_back(0);
            }

            ++numero;
        }

        unsigned int m=0;
        double sarakeOKsumma=0;
        while(m<sarakeOK.size()){
            sarakeOKsumma=sarakeOKsumma+sarakeOK.at(m);
            ++m;
        }
        sudokuOK.push_back(sarakeOKsumma);
        ++n;
    }

    unsigned int m=0;
    double sudokuOKsumma=0;
    while(m<sudokuOK.size()){
        sudokuOKsumma=sudokuOKsumma+sudokuOK.at(m);
        ++m;

    }

    double sarakehyvyys=sudokuOKsumma/81.0;

    return sarakehyvyys;
}

double sudoku::haeNelioHyvyysarvo(std::vector<std::vector<int> > tulostettavaSudoku)
{
    /* Sudoku on ratkaistu, jos:
     * 1. Jokaisella rivillä esiintyy numerot 1-9 kerran ja vain kerran. Eli rivisumma 45.
     * 2. Jokaisessa sarakkeessa esiintyy numerot 1-9 kerran ja vain kerran. Eli sarakesumma 45.
     * 3. Jokaisessa 3x3 osamatriisissa sisältyy numerot 1-9 kerran ja vain kerran. Eli osasumma 45.

    Muodostetaan näiden ehtojen pohjalta ratkaisuehdotukselle hyvyysarvo.
    Ratkaisuehdotuksen generoinnissa on jo huomioitu, ettei yhdellä rivillä ole tietty numero kuin kerran,
    näin ollen ehtoa 1. ei tarvitse huomioida.

    2. Tutkitaan nyt, onko jokaisessa 3x3 osamatriisi neliössä kaikki luvut 1-9 kerran ja vain kerran.
    Jos luku löytyy kerran, lisätään nelioOKsummaan +1. Tämän jälkeen lasketaan kaikkien ratkaisuehdokas sudokun neliöiden
    summat yhteen ja lasketaan tämän perusteella neliöhyvyys=saatu summa/81.

    Osaneliöt on numeroitu:

    1. | 2. | 3.
    4. | 5. | 6.
    7. | 8. | 9.
    */
   std::vector<int> sudokuOK;
   unsigned int i=0;

   while(i<tulostettavaSudoku.size()){
       std::vector<int> nelioOK;
       //Haetaan ratkaisuehdokkaasta tietyn neliön luvut.
       std::vector<int> nelionumerot= haeNelionKaytetytLuvut(i+1,tulostettavaSudoku);
       //Tutkii löytyykö numerot 1-9 tasan kerran.
       unsigned int numero=1;
       while(numero<=9){
           unsigned int j=0;
           int montako=0;
           while(j<nelionumerot.size()){
               if(nelionumerot.at(j)==numero){
                    //Jos esim. numero 1 löytyy sarakkeelta, lisätään ++montako.
                   ++montako;
               }
               ++j;
           }
           if(montako==1){
               //Jos tietty numero löytyy sarakkeesta kerran, tallennetaan 1.
               nelioOK.push_back(1);
           }
           else{
               //Jos tietty numero löytyy sarakkeesta jotain muuta kuin tasan kerran, tallennetaan 0.
               nelioOK.push_back(0);
           }

           ++numero;
       }

       unsigned int m=0;
       double nelioOKsumma=0;
       while(m<nelioOK.size()){
           nelioOKsumma=nelioOKsumma+nelioOK.at(m);
           ++m;
       }
       sudokuOK.push_back(nelioOKsumma);
       ++i;

   }

   unsigned int m=0;
   double sudokuOKsumma=0;
   while(m<sudokuOK.size()){
       sudokuOKsumma=sudokuOKsumma+sudokuOK.at(m);
       ++m;

   }

   double neliohyvyys=sudokuOKsumma/81;

   return neliohyvyys;

}

double sudoku::haeHyvyysarvo(std::vector<std::vector<int> > tulostettavaSudoku)
{
    double hyvyysarvo = haeNelioHyvyysarvo(tulostettavaSudoku)
            *haeSarakeHyvyysarvo(tulostettavaSudoku);

    return hyvyysarvo;
}

