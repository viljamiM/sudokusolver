TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    sudoku.cpp \
    generointi.cpp

HEADERS += \
    sudoku.h \
    generointi.h
