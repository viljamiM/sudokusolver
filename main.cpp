#include <iostream>
#include <vector>
#include "sudoku.h"
#include "generointi.h"

using namespace std;

int main()
{

    //Muodostetaan ratkaistava sudoku siten että luodaan vector, jonka
    //alkioina on vectoreita, jotka kuvaavat sudokun vaakarivejä
    //ja luvut 0 kuvaavat tyhjiä ruutuja.
    std::vector<vector<int>> huippuhelppoSudoku1={
        {3,0,2,0,6,5,0,4,7},
        {0,5,0,0,3,4,1,8,2},
        {0,9,0,1,7,0,0,6,0},
        {2,0,6,0,9,0,3,0,8},
        {1,0,0,0,0,0,2,7,0},
        {0,8,9,5,0,3,4,0,0},
        {4,7,0,3,0,0,0,0,5},
        {0,0,0,6,4,8,0,9,1},
        {0,0,0,2,0,0,0,0,0}
    };
    std::vector<vector<int>> huippuhelppoSudoku2={
        {0,0,8,0,6,0,9,0,0},
        {0,0,0,2,0,3,6,7,8},
        {7,0,6,0,5,1,0,0,4},
        {9,7,3,0,4,8,1,0,0},
        {6,2,0,0,3,9,0,5,0},
        {0,0,1,7,0,0,0,0,0},
        {5,8,0,9,0,0,3,0,6},
        {0,0,0,0,0,0,0,0,0},
        {0,4,0,0,0,5,7,2,1}
    };
    std::vector<vector<int>> huippuhelppoSudoku3={
        {0,4,0,0,0,0,1,7,9},
        {0,0,2,0,0,8,0,5,4},
        {0,0,6,0,0,5,0,0,8},
        {0,8,0,0,7,0,9,1,0},
        {0,5,0,0,9,0,0,3,0},
        {0,1,9,0,6,0,0,4,0},
        {3,0,0,4,0,0,7,0,0},
        {5,7,0,1,0,0,2,0,0},
        {9,2,8,0,0,0,0,6,0}
    };
    std::vector<vector<int>> keskivaikeaSudoku1={
        {2,0,6,0,0,0,0,4,9},
        {0,3,7,0,0,9,0,0,0},
        {1,0,0,7,0,0,0,0,6},
        {0,0,0,5,8,0,9,0,0},
        {7,0,5,0,0,0,8,0,4},
        {0,0,9,0,6,2,0,0,0},
        {9,0,0,0,0,4,0,0,1},
        {0,0,0,3,0,0,4,9,0},
        {4,1,0,0,0,0,2,0,8}
    };
    std::vector<vector<int>> keskivaikeaSudoku4={
        {1,0,5,0,0,0,3,7,0},
        {0,0,0,0,0,0,2,0,0},
        {0,9,7,3,0,0,0,1,0},
        {0,0,0,0,5,3,1,0,2},
        {3,0,0,8,0,1,0,0,4},
        {2,0,1,4,7,0,0,0,0},
        {0,7,0,0,0,8,6,4,0},
        {0,0,8,0,0,0,0,0,0},
        {0,1,2,0,0,0,8,0,7}
    };
    std::vector<vector<int>> huippuvaikeaSudoku1={
        {7,9,0,0,0,0,0,0,3},
        {0,0,0,0,0,0,0,6,0},
        {8,0,1,0,0,4,0,0,2},
        {0,0,5,0,0,0,0,0,0},
        {3,0,0,1,0,0,0,0,0},
        {0,4,0,0,0,6,2,0,9},
        {2,0,0,0,3,0,0,0,6},
        {0,3,0,6,0,5,4,2,1},
        {0,0,0,0,0,0,0,0,0}
    };
    std::vector<vector<int>> erittainVaikeaSudoku={
        {0,0,0,0,0,3,0,1,7},
        {0,1,5,0,0,9,0,0,8},
        {0,6,0,0,0,0,0,0,0},
        {1,0,0,0,0,7,0,0,0},
        {0,0,9,0,0,0,2,0,0},
        {0,0,0,5,0,0,0,0,4},
        {0,0,0,0,0,0,0,2,0},
        {5,0,7,5,0,0,3,4,0},
        {3,4,0,3,4,0,0,0,0}
    };
    std::vector<vector<int>> maailmanVaikeinSudoku={
        {1,0,0,0,0,7,0,9,0},
        {0,3,0,0,2,0,0,0,8},
        {0,0,9,6,0,0,5,0,0},
        {0,0,5,3,0,0,9,0,0},
        {0,1,0,0,8,0,0,0,2},
        {6,0,0,0,0,4,0,0,0},
        {3,0,0,0,0,0,0,1,0},
        {0,4,0,0,0,0,0,0,7},
        {0,0,7,0,0,0,3,0,0}
    };
    std::cout<<"----------------------------------------------"<<std::endl;
    std::cout<<"Ratkaistaan Huippuhelppo sudoku1:"<<std::endl;
    sudoku huippuhelppo1 = sudoku(huippuhelppoSudoku1);
    int kierrokset = 1000;
    int eliittienMaara = 10;
    int populaationSuuruus = 100;
    int maksimiSamaHyvyysarvo = 100;
    double valintaArvo = 0.75;
    double mutaatioArvo = 0.75;
    huippuhelppo1.ratkaiseSudoku(kierrokset,eliittienMaara,populaationSuuruus
                                 ,mutaatioArvo,valintaArvo,maksimiSamaHyvyysarvo);

    std::cout<<"----------------------------------------------"<<std::endl;
    std::cout<<"Ratkaistaan Huippuhelppo sudoku 2:"<<std::endl;
    sudoku huippuhelppo2 = sudoku(huippuhelppoSudoku2);
    kierrokset = 1000;
    eliittienMaara = 10;
    populaationSuuruus = 100;
    maksimiSamaHyvyysarvo = 100;
    valintaArvo = 0.75;
    mutaatioArvo = 0.75;
    huippuhelppo2.ratkaiseSudoku(kierrokset,eliittienMaara,populaationSuuruus
                                 ,mutaatioArvo,valintaArvo,maksimiSamaHyvyysarvo);

    std::cout<<"----------------------------------------------"<<std::endl;
    std::cout<<"Ratkaistaan Huippuhelppo sudoku 3:"<<std::endl;
    sudoku huippuhelppo3 = sudoku(huippuhelppoSudoku3);
    kierrokset = 100;
    eliittienMaara = 100;
    populaationSuuruus = 1000;
    maksimiSamaHyvyysarvo = 50;
    valintaArvo = 0.85;
    mutaatioArvo = 0.85;
    huippuhelppo3.ratkaiseSudoku(kierrokset,eliittienMaara,populaationSuuruus
                                 ,mutaatioArvo,valintaArvo,maksimiSamaHyvyysarvo);

    std::cout<<"----------------------------------------------"<<std::endl;
    std::cout<<"Ratkaistaan keskivaikea sudoku 1:"<<std::endl;
    sudoku keskivaikea1 = sudoku(keskivaikeaSudoku1);
    kierrokset = 1000;
    eliittienMaara = 10;
    populaationSuuruus = 100;
    maksimiSamaHyvyysarvo = 100;
    valintaArvo = 0.85;
    mutaatioArvo = 0.85;
    keskivaikea1.ratkaiseSudoku(kierrokset,eliittienMaara,populaationSuuruus
                               ,mutaatioArvo,valintaArvo,maksimiSamaHyvyysarvo);

    std::cout<<"----------------------------------------------"<<std::endl;
    std::cout<<"Ratkaistaan keskivaikea sudoku 4:"<<std::endl;
    sudoku keskivaikea4 = sudoku(keskivaikeaSudoku4);
    kierrokset = 1000;
    eliittienMaara = 10;
    populaationSuuruus = 100;
    maksimiSamaHyvyysarvo = 100;
    valintaArvo = 0.85;
    mutaatioArvo = 0.85;
    keskivaikea4.ratkaiseSudoku(kierrokset,eliittienMaara,populaationSuuruus
                               ,mutaatioArvo,valintaArvo,maksimiSamaHyvyysarvo);

    std::cout<<"----------------------------------------------"<<std::endl;
    std::cout<<"Ratkaistaan huippu vaikea sudoku:"<<std::endl;
    sudoku huippuvaikea = sudoku(huippuvaikeaSudoku1);
    kierrokset = 1000;
    eliittienMaara = 10;
    populaationSuuruus = 100;
    maksimiSamaHyvyysarvo = 100;
    valintaArvo = 0.85;
    mutaatioArvo = 0.85;
    huippuvaikea.ratkaiseSudoku(kierrokset,eliittienMaara,populaationSuuruus
                                ,mutaatioArvo,valintaArvo,maksimiSamaHyvyysarvo);

    std::cout<<"----------------------------------------------"<<std::endl;
    std::cout<<"Ratkaistaan erittain vaikea sudoku:"<<std::endl;
    sudoku erittainvaikea = sudoku(erittainVaikeaSudoku);
    kierrokset = 1000;
    eliittienMaara = 10;
    populaationSuuruus = 100;
    maksimiSamaHyvyysarvo = 100;
    valintaArvo = 0.85;
    mutaatioArvo = 0.85;
    erittainvaikea.ratkaiseSudoku(kierrokset,eliittienMaara,populaationSuuruus
                                 ,mutaatioArvo,valintaArvo,maksimiSamaHyvyysarvo);

    std::cout<<"----------------------------------------------"<<std::endl;
    std::cout<<"Ratkaistaan maailman vaikein sudoku:"<<std::endl;
    sudoku maailmanVaikein = sudoku(maailmanVaikeinSudoku);
    kierrokset = 1000;
    eliittienMaara = 10;
    populaationSuuruus = 100;
    maksimiSamaHyvyysarvo = 100;
    valintaArvo = 0.85;
    mutaatioArvo = 0.85;
    maailmanVaikein.ratkaiseSudoku(kierrokset,eliittienMaara,populaationSuuruus
                                   ,mutaatioArvo,valintaArvo,maksimiSamaHyvyysarvo);
}
